package com.raone.greenlab.ui.main.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.raone.greenlab.R
import com.raone.greenlab.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_main.*

/**
 * MainFragment.
 * Manifested by r1.
 */
class MainFragment : BaseFragment() {
    private var listener: OnMainFragmentListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnMainFragmentListener) {
            listener = context
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rlGrass1.setOnClickListener {
            listener?.firstSelected()
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnMainFragmentListener {
        fun firstSelected()
    }

    companion object {
        const val TAG = "ManFragment"
        fun instance() = MainFragment()
    }
}
