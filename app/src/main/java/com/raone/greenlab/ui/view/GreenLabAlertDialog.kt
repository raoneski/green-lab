package com.raone.greenlab.ui.view

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.raone.greenlab.R
import kotlinx.android.synthetic.main.alert_dialog_green_lab.view.*

class GreenLabAlertDialog (context: Context) : BaseDialogHelper() {

    //  dialog view
    override val dialogView: View by lazy {
        LayoutInflater.from(context).inflate(R.layout.alert_dialog_green_lab, null)
    }

    override val builder: AlertDialog.Builder = AlertDialog.Builder(context).setView(dialogView)

    // Views
    private val tvTitle: TextView by lazy { dialogView.ad_tv_title }
    private val tvBody: TextView by lazy { dialogView.ad_tv_body }
    private val llBtnContainer: LinearLayout by lazy { dialogView.ad_ll_btn_container }
    private val btnNegative: Button by lazy {dialogView.ad_btn_negative}
    private val btnPositive: Button by lazy {dialogView.ad_btn_positive}
    private val cbUseFurther: CheckBox by lazy { dialogView.ad_cb_use_further }
    private val icIcon: ImageView by lazy { dialogView.ad_ic_icon }

    var raone = 0

    //  Negative button click listener
    fun negativeClickListener(func: (() -> Unit)? = null) =
        with(btnNegative) {
            setClickListenerToDialogIcon(func)
        }

    fun positiveClickListener(func: (() -> Unit)? = null ) =
        with(btnPositive) {
            setClickListenerToDialogIcon(func)
        }


    //  view click listener as extension function
    private fun View.setClickListenerToDialogIcon(func: (() -> Unit)?) =
        setOnClickListener {
            dialog?.dismiss()
            Log.i(
                "EasyPermissions >>>>",
                "dialog?.dismiss()"
            )

            func?.invoke()
        }


    fun setTitle(title: String) {
        tvTitle.visibility = View.VISIBLE
        tvTitle.text = title
    }

    fun setMessage(body: String) {
        tvBody.visibility = View.VISIBLE
        tvBody.text = body
    }

    fun showNegativeButton(text: String) {
        llBtnContainer.visibility = View.VISIBLE

        btnNegative.text = text
        btnNegative.visibility = View.VISIBLE
    }

    fun showPositiveButton(text: String) {
        llBtnContainer.visibility = View.VISIBLE

        btnPositive.text = text
        btnPositive.visibility = View.VISIBLE
    }

    fun showCheckBox(text: String) {
        cbUseFurther.text = text
        cbUseFurther.visibility = View.VISIBLE
    }

    fun isChecked(): Boolean {
        return cbUseFurther.isChecked
    }

    fun setIcon(resId: Int) {
        icIcon.setImageResource(resId)
    }
}
