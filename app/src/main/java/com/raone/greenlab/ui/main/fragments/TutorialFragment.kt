package com.raone.greenlab.ui.main.fragments


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.ViewPager

import com.raone.greenlab.R
import com.raone.greenlab.base.BaseFragment
import com.raone.greenlab.ui.adapter.TutorialViewPagerAdapter
import kotlinx.android.synthetic.main.fragment_tutorial.*

/**
 * Tutorial fragment.
 * Created by r1.
 */
class TutorialFragment : BaseFragment() {
    private var listener: OnTutorialFragmentListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnTutorialFragmentListener) {
            listener = context
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tutorial, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = TutorialViewPagerAdapter(activity, arrayListOf(
            R.layout.tutorial_one,
            R.layout.tutorial_two,
            R.layout.tutorial_three
        ))

        vpTutorial.adapter = adapter

        btnNext.setOnClickListener {
            var item = vpTutorial.currentItem

            item++
            if (item >= 3) {
                listener?.endTutorial()
            }

            viewPagerItemSelected(item)

            vpTutorial.currentItem = item
        }

        vpTutorial.addOnPageChangeListener(object: ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                viewPagerItemSelected(position)
            }

        })

        firstTutorialSelected()
    }

    private fun viewPagerItemSelected(item: Int) {
        when (item) {
            0 -> {
                firstTutorialSelected()
            }

            1 -> {
                secondTutorialSelected()
            }

            2 -> {
                thirdTutorialSelected()
            }
        }
    }


    private fun thirdTutorialSelected() {
        vpItemOne.setBackgroundResource(R.drawable.view_pager_outline)
        vpItemTwo.setBackgroundResource(R.drawable.view_pager_outline)
        vpItemThree.setBackgroundResource(R.drawable.view_pager_selected)
    }

    private fun secondTutorialSelected() {
        vpItemOne.setBackgroundResource(R.drawable.view_pager_outline)
        vpItemTwo.setBackgroundResource(R.drawable.view_pager_selected)
        vpItemThree.setBackgroundResource(R.drawable.view_pager_outline)
    }

    private fun firstTutorialSelected() {
        vpItemOne.setBackgroundResource(R.drawable.view_pager_selected)
        vpItemTwo.setBackgroundResource(R.drawable.view_pager_outline)
        vpItemThree.setBackgroundResource(R.drawable.view_pager_outline)
    }

    interface OnTutorialFragmentListener {
        fun endTutorial()
    }

    companion object {
        fun instance() = TutorialFragment()
    }
}
