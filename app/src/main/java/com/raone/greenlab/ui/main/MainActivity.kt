package com.raone.greenlab.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.viewpager.widget.ViewPager
import com.raone.greenlab.R
import com.raone.greenlab.base.BaseFragment
import com.raone.greenlab.manager.SettingsManager
import com.raone.greenlab.presenter.SettingsPresenter
import com.raone.greenlab.ui.adapter.TutorialViewPagerAdapter
import com.raone.greenlab.ui.main.fragments.CalendarFragment
import com.raone.greenlab.ui.main.fragments.MainFragment
import com.raone.greenlab.ui.main.fragments.StartFragment
import com.raone.greenlab.ui.main.fragments.TutorialFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),
    StartFragment.OnStartFragmentListener,
    TutorialFragment.OnTutorialFragmentListener,
    MainFragment.OnMainFragmentListener {
    private val settingsPresenter = SettingsPresenter()

    private val startFragment = StartFragment.instance()
    private val tutorialFragment = TutorialFragment.instance()
    private val mainFragment = MainFragment.instance()
    private val calendarFragment = CalendarFragment.instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (!settingsPresenter.loadBoolean(SettingsManager.MAIN_SHOW_START_FRAGMENT)) {
            addFragment(startFragment, getString(R.string.fragment_start))
        } else {
            replaceFragment(mainFragment, getString(R.string.fragment_main))
        }
    }

    private fun addFragment(fragment: BaseFragment, tag: String) {
        supportFragmentManager
            .beginTransaction()
            .add(R.id.mainContainer, fragment, tag)
            .commit()
    }

    private fun replaceFragmentBackStack(fragment: BaseFragment, tag: String) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.mainContainer, fragment, tag)
            .addToBackStack(tag)
            .commit()
    }


    private fun replaceFragment(fragment: BaseFragment, tag: String) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.mainContainer, fragment, tag)
            .commit()
    }

    private fun removeFragment(fragment: BaseFragment) {
        supportFragmentManager
            .beginTransaction()
            .remove(fragment)
            .commit()
    }

    override fun start() {
        replaceFragmentBackStack(tutorialFragment, getString(R.string.fragment_tutorial))
    }

    override fun endTutorial() {
        removeFragment(startFragment)
        removeFragment(tutorialFragment)
        settingsPresenter.saveBoolean(SettingsManager.MAIN_SHOW_START_FRAGMENT, true)
        addFragment(mainFragment, getString(R.string.fragment_main))
    }

    override fun firstSelected() {
        replaceFragment(calendarFragment, getString(R.string.fragment_calendar))
    }
}
