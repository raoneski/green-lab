package com.raone.greenlab.ui.test

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.raone.greenlab.GreenLab
import com.raone.greenlab.R
import com.raone.greenlab.manager.SettingsManager.Companion.SETTINGS_TEST_CACHE
import com.raone.greenlab.presenter.SettingsPresenter
import kotlinx.android.synthetic.main.activity_test.*

class TestActivity : AppCompatActivity() {
    private val settingsPresenter = SettingsPresenter()

    init {
        GreenLab.instance.getGreenLabComponent().inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)

        btnSaveAndGet.setOnClickListener {
            val text = etCache.text.toString()

            settingsPresenter.saveString(SETTINGS_TEST_CACHE, text)

            Log.i(TAG, "Cache value = ${settingsPresenter.loadString(SETTINGS_TEST_CACHE)}")
        }
    }

    companion object {
        const val TAG = "TestActivity"
    }
}
