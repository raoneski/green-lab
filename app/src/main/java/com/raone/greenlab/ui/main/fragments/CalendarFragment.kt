package com.raone.greenlab.ui.main.fragments


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.raone.greenlab.GreenLabAlertDialog

import com.raone.greenlab.R
import com.raone.greenlab.base.BaseFragment
import com.raone.greenlab.modules.Calendar
import com.raone.greenlab.ui.adapter.CalendarAdapter
import kotlinx.android.synthetic.main.fragment_calendar.*

/**
 * CalendarFragment.
 * Manifested by r1.
 */
class CalendarFragment : BaseFragment(), CalendarAdapter.OnCalendarAdapterListener {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_calendar, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ibInstruction.setOnClickListener {
            GreenLabAlertDialog {
                setIcon(R.drawable.ic_open_wrench_tool_silhouette)
                setTitle(getString(R.string.dialog_instruction_title))
                setMessage(getString(R.string.dialog_instruction_desc))
                showPositiveButton(getString(R.string.ok))

                positiveClickListener {
                    Log.i(TAG, "ibInstruction: positiveClickListener: ")
                }
            }.show()
        }

        ibQuestion.setOnClickListener {
            GreenLabAlertDialog {
                setIcon(R.drawable.ic_question)
                setTitle(getString(R.string.dialog_info_title_1))
                setMessage(getString(R.string.dialog_info_desc_1))
                showPositiveButton(getString(R.string.ok))

                positiveClickListener {
                    Log.i(TAG, "ibQuestion: positiveClickListener: ")
                }
            }.show()
        }

        val layoutManager = LinearLayoutManager(activity)
        rvCalendar.layoutManager = layoutManager
        rvCalendar.hasFixedSize()

        generateDataAndSetAdapter()
    }

    private fun generateDataAndSetAdapter() {
        if (rvCalendar == null) return

        val items: ArrayList<ArrayList<Calendar>> = arrayListOf()

        var id = 0
        for (i in 0..8) {
            val list = arrayListOf<Calendar>()
            for (j in 0..4) {
                id ++
                var action = 0

                if (id % 2 == 0) action = 1
                if (id == 9) action = 3
                if (id == 16) action = 3
                if (id == 29) action = 4
                if (id % 6 == 0) action = 2
                if (id == 45) action = 5

                val calendar = Calendar(id, action)

                Log.i(TAG, "generateDataAndSetAdapter: calendar=$calendar")
                list.add(calendar)
            }

            items.add(list)
        }

        val adapter = CalendarAdapter(items)
        adapter.listener = this
        rvCalendar.adapter = adapter
    }

    override fun calendarItemClicked(position: Int, calendar: Calendar) {
        var text = ""
        if (calendar.id % 2 == 0) {
            text += getString(R.string.action_1)
        }
        if (calendar.id == 9) {
            text += getString(R.string.action_2)
        }

        if (calendar.id == 16) {
            text += getString(R.string.action_3)
        }
        if (calendar.id == 29) {
            text += getString(R.string.action_4)
        }
        if (calendar.id % 6 == 0) {
            text += getString(R.string.action_6)
        }
        if (calendar.id == 45) {
            text += getString(R.string.action_5)
        }

        if (text.isEmpty()) text = getString(R.string.action_7)

        GreenLabAlertDialog {
            setIcon(R.drawable.ic_question)
            setTitle(getString(R.string.what_to_do))
            setMessage(text)
            showPositiveButton(getString(R.string.ok))
            positiveClickListener {

                Log.i(TAG, "generateDataAndSetAdapter: positiveClickListener: position=$position, calendar=$calendar")
            }

        }.show()

        Log.i(TAG, "generateDataAndSetAdapter: position=$position, calendar=$calendar")
    }

    companion object {
        const val TAG = "CalendarFragment"
        fun instance() = CalendarFragment()
    }
}
