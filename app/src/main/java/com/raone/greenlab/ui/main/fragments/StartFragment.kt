package com.raone.greenlab.ui.main.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.raone.greenlab.R
import com.raone.greenlab.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_start.*

/**
 * StartFragment.
 * Create by r1.
 */
class StartFragment : BaseFragment() {
    private var listener: OnStartFragmentListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_start, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnStart.setOnClickListener {
            listener?.start()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnStartFragmentListener) {
            listener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    interface OnStartFragmentListener {
        fun start()
    }

    companion object {
        fun instance() = StartFragment()
    }
}
