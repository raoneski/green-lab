package com.raone.greenlab.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.raone.greenlab.R
import com.raone.greenlab.modules.Calendar
import kotlinx.android.synthetic.main.item_calendar.view.*

class CalendarAdapter(var items: ArrayList<ArrayList<Calendar>>): RecyclerView.Adapter<CalendarAdapter.ViewHolder>() {
    var listener: OnCalendarAdapterListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_calendar, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setData(items[position])
    }

    inner class ViewHolder(val v: View): RecyclerView.ViewHolder(v) {

        fun setData(array: ArrayList<Calendar>) {

            v.rlCalendarContainer1.setOnClickListener {
                listener?.calendarItemClicked(adapterPosition, array[0])
            }
            v.rlCalendarContainer2.setOnClickListener {
                listener?.calendarItemClicked(adapterPosition, array[1])
            }
            v.rlCalendarContainer3.setOnClickListener {
                listener?.calendarItemClicked(adapterPosition, array[2])
            }
            v.rlCalendarContainer4.setOnClickListener {
                listener?.calendarItemClicked(adapterPosition, array[3])
            }
            v.rlCalendarContainer5.setOnClickListener {
                listener?.calendarItemClicked(adapterPosition, array[4])
            }

            v.tvDay1.text = array[0].id.toString()
            when (array[0].action) {
                0 -> {
                    v.icAction1.visibility = View.GONE
                }

                1 -> {
                    v.icAction1.visibility = View.VISIBLE
                    v.icAction1.setImageResource(R.drawable.ic_water_drop)
                }

                2 -> {
                    v.icAction1.visibility = View.VISIBLE
                    v.icAction1.setImageResource(R.drawable.ic_sunny)
                }

                3 -> {
                    v.icAction1.visibility = View.VISIBLE
                    v.icAction1.setImageResource(R.drawable.ic_eye)

                }

                4 -> {
                    v.icAction1.visibility = View.VISIBLE
                    v.icAction1.setImageResource(R.drawable.ic_fertilizer)
                }

                5 -> {
                    v.icAction1.visibility = View.VISIBLE
                    v.icAction1.setImageResource(R.drawable.ic_scissors)
                }
            }

            v.tvDay2.text = array[1].id.toString()
            when (array[1].action) {
                0 -> {
                    v.icAction2.visibility = View.GONE
                }

                1 -> {
                    v.icAction2.visibility = View.VISIBLE
                    v.icAction2.setImageResource(R.drawable.ic_water_drop)
                }

                2 -> {
                    v.icAction2.visibility = View.VISIBLE
                    v.icAction2.setImageResource(R.drawable.ic_sunny)
                }

                3 -> {
                    v.icAction2.visibility = View.VISIBLE
                    v.icAction2.setImageResource(R.drawable.ic_eye)
                }

                4 -> {
                    v.icAction2.visibility = View.VISIBLE
                    v.icAction2.setImageResource(R.drawable.ic_fertilizer)
                }

                5 -> {
                    v.icAction2.visibility = View.VISIBLE
                    v.icAction2.setImageResource(R.drawable.ic_scissors)
                }
            }

            v.tvDay3.text = array[2].id.toString()
            when (array[2].action) {
                0 -> {
                    v.icAction3.visibility = View.GONE
                }

                1 -> {
                    v.icAction3.visibility = View.VISIBLE
                    v.icAction3.setImageResource(R.drawable.ic_water_drop)
                }

                2 -> {
                    v.icAction3.visibility = View.VISIBLE
                    v.icAction3.setImageResource(R.drawable.ic_sunny)
                }

                3 -> {
                    v.icAction3.visibility = View.VISIBLE
                    v.icAction3.setImageResource(R.drawable.ic_eye)

                }

                4 -> {
                    v.icAction3.visibility = View.VISIBLE
                    v.icAction3.setImageResource(R.drawable.ic_fertilizer)
                }

                5 -> {
                    v.icAction3.visibility = View.VISIBLE
                    v.icAction3.setImageResource(R.drawable.ic_scissors)
                }
            }

            v.tvDay4.text = array[3].id.toString()
            when (array[3].action) {
                0 -> {
                    v.icAction4.visibility = View.GONE
                }

                1 -> {
                    v.icAction4.visibility = View.VISIBLE
                    v.icAction4.setImageResource(R.drawable.ic_water_drop)
                }

                2 -> {
                    v.icAction4.visibility = View.VISIBLE
                    v.icAction4.setImageResource(R.drawable.ic_sunny)
                }

                3 -> {
                    v.icAction4.visibility = View.VISIBLE
                    v.icAction4.setImageResource(R.drawable.ic_eye)

                }

                4 -> {
                    v.icAction4.visibility = View.VISIBLE
                    v.icAction4.setImageResource(R.drawable.ic_fertilizer)
                }

                5 -> {
                    v.icAction4.visibility = View.VISIBLE
                    v.icAction4.setImageResource(R.drawable.ic_scissors)
                }
            }

            v.tvDay5.text = array[4].id.toString()
            when (array[4].action) {
                0 -> {
                    v.icAction5.visibility = View.GONE
                }

                1 -> {
                    v.icAction5.visibility = View.VISIBLE
                    v.icAction5.setImageResource(R.drawable.ic_water_drop)
                }

                2 -> {
                    v.icAction5.visibility = View.VISIBLE
                    v.icAction5.setImageResource(R.drawable.ic_sunny)
                }

                3 -> {
                    v.icAction5.visibility = View.VISIBLE
                    v.icAction5.setImageResource(R.drawable.ic_eye)

                }

                4 -> {
                    v.icAction5.visibility = View.VISIBLE
                    v.icAction5.setImageResource(R.drawable.ic_fertilizer)
                }

                5 -> {
                    v.icAction5.visibility = View.VISIBLE
                    v.icAction5.setImageResource(R.drawable.ic_scissors)
                }
            }

            // On click.
            v.rlCalendarContainer1.setOnClickListener {
                listener?.calendarItemClicked(adapterPosition, array[0])
            }
            v.rlCalendarContainer2.setOnClickListener {
                listener?.calendarItemClicked(adapterPosition, array[1])
            }
            v.rlCalendarContainer3.setOnClickListener {
                listener?.calendarItemClicked(adapterPosition, array[2])
            }
            v.rlCalendarContainer4.setOnClickListener {
                listener?.calendarItemClicked(adapterPosition, array[3])
            }
            v.rlCalendarContainer5.setOnClickListener {
                listener?.calendarItemClicked(adapterPosition, array[4])
            }
        }
    }

    interface OnCalendarAdapterListener {
        fun calendarItemClicked(position: Int, calendar: Calendar)
    }
}