package com.raone.greenlab.modules

data class Calendar (
    val id: Int = 0,
    val action: Int = 0
)