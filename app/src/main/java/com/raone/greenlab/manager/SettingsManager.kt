package com.raone.greenlab.manager

import android.content.SharedPreferences
import com.raone.greenlab.interactor.ISettingsPreferences

class SettingsManager(val pref: SharedPreferences): ISettingsPreferences {

    override fun saveInt(key: String, value: Int) {
        edit()
            .putInt(key, value)
            .apply()
    }

    override fun loadInt(key: String): Int {
        return pref.getInt(key, 0)
    }

    override fun saveFloat(key: String, value: Float) {
        edit()
            .putFloat(key, value)
            .apply()
    }

    override fun loadFloat(key: String): Float {
        return pref.getFloat(key, 0.0f)
    }

    override fun saveBoolean(key: String, value: Boolean) {
        edit()
            .putBoolean(key, value)
            .apply()
    }

    override fun loadBoolean(key: String): Boolean {
        return pref.getBoolean(key, false)
    }

    override fun saveLong(key: String, value: Long) {
        edit()
            .putLong(key, value)
            .apply()
    }

    override fun loadLong(key: String): Long {
        return pref.getLong(key, 0.toLong())
    }

    override fun saveString(key: String, value: String) {
        edit()
            .putString(key, value)
            .apply()
    }

    override fun loadString(key: String): String {
        return pref.getString(key, "")!!
    }

    private fun edit() = pref.edit()

    companion object {
        const val SETTINGS_TEST_CACHE = "trfXkkex3gwkyAsP5hP5M6"
        const val MAIN_SHOW_START_FRAGMENT = "yFjFCcYZLkx2hy65ugNXBU"
    }
}

// Keys.
//4ehA3GNuUtRwCGyQRagzu6
//zGskpVgz7ehWTB6GswY4pd
//tFdpF4DG2thEqqa8trg4ct
//FEagp9CvdnbnANGSwCNf7j
//8Y2VE4MGgLTz9PwFXd4hXf
//vJ9XG2ubEeBp8DnAjTJuUc
//F6tebEMvjxZcjj3fRa5ufn
//dJW6FXYcBPF5HU9sX8dB8C
//GCQ8vE9PLNqbuntJ9m7GuR
//cvxdx48cvnmnNTG6GwXGJE
//WaKThQjHC58aXRgdqH2CAg
//ZypmYtJTMzWk56XErVEkEt
//u7GwKjDqxjQWVrwn2YVYpG
//8aVBtyzHUMuJM3K7wDVFBJ
//pTXqEVGRgvVuHpuxVXZMdW
//Q58ef3ghFyYsN3bccm7k8D
//n59RMJSh8YsB7HX7udbMLn
//jYaLzbQmwZCu6dgPvUcKCP
//efvG3rfRQbtMYEDkk7wuy8
//n77rUPTLmUsdvHUzmSzULL