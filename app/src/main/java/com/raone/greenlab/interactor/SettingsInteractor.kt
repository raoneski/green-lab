package com.raone.greenlab.interactor

import com.raone.greenlab.manager.SettingsManager

class SettingsInteractor(val manager: SettingsManager): ISettingsInteractor {
    override fun saveInt(key: String, value: Int) {
        manager.saveInt(key, value)
    }

    override fun loadInt(key: String): Int {
        return manager.loadInt(key)
    }

    override fun saveFloat(key: String, value: Float) {
        manager.saveFloat(key, value)
    }

    override fun loadFloat(key: String): Float {
        return manager.loadFloat(key)
    }

    override fun saveBoolean(key: String, value: Boolean) {
        manager.saveBoolean(key, value)
    }

    override fun loadBoolean(key: String): Boolean {
        return manager.loadBoolean(key)
    }

    override fun saveLong(key: String, value: Long) {
        manager.saveLong(key, value)
    }

    override fun loadLong(key: String): Long {
        return manager.loadLong(key)
    }

    override fun saveString(key: String, value: String) {
        manager.saveString(key, value)
    }

    override fun loadString(key: String): String {
        return manager.loadString(key)
    }

}

interface ISettingsInteractor: ISettingsPreferences {

}

interface ISettingsPreferences {
    fun saveInt(key: String, value: Int)
    fun loadInt(key: String): Int

    fun saveFloat(key: String, value: Float)
    fun loadFloat(key: String): Float

    fun saveBoolean(key: String, value: Boolean)
    fun loadBoolean(key: String): Boolean

    fun saveLong(key: String, value: Long)
    fun loadLong(key: String): Long

    fun saveString(key: String, value: String)
    fun loadString(key: String): String
}