package com.raone.greenlab.di.module

import android.content.SharedPreferences
import com.raone.greenlab.interactor.SettingsInteractor
import com.raone.greenlab.manager.SettingsManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class InteractorModule {

    @Provides
    @Singleton
    fun provideSettingsInteractor(manager: SettingsManager) = SettingsInteractor(manager)
}