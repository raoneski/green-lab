package com.raone.greenlab.di.module

import android.content.SharedPreferences
import com.raone.greenlab.manager.SettingsManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ManagerModule {

    @Provides
    @Singleton
    fun provideSettingsManager(preferences: SharedPreferences) = SettingsManager(preferences)
}