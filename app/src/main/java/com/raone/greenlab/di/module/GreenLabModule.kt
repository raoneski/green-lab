package com.raone.greenlab.di.module

import android.app.Application
import android.content.Context
import com.raone.greenlab.GreenLab
import com.raone.greenlab.util.GREEN_LAB_PREFERENCES
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class GreenLabModule (
    private val greenLab: GreenLab
) {
    @Provides
    @Singleton
    fun provideApplication(): Application {
        return greenLab
    }

    @Provides
    fun provideSharedPreferences() = greenLab.getSharedPreferences(GREEN_LAB_PREFERENCES, Context.MODE_PRIVATE)

}