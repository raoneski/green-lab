package com.raone.greenlab.di.componets

import com.raone.greenlab.GreenLab
import com.raone.greenlab.di.module.GreenLabModule
import com.raone.greenlab.di.module.InteractorModule
import com.raone.greenlab.di.module.ManagerModule
import com.raone.greenlab.interactor.SettingsInteractor
import com.raone.greenlab.presenter.SettingsPresenter
import com.raone.greenlab.ui.main.MainActivity
import com.raone.greenlab.ui.test.TestActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    GreenLabModule::class,
    ManagerModule::class,
    InteractorModule::class])
interface GreenLabComponent {
    fun inject(application: GreenLab)

    // Interactor.
    fun inject(settingsInteractor: SettingsInteractor)

    // Presenter.
    fun inject(settingsPresenter: SettingsPresenter)

    // Activity.
    fun inject(mainActivity: MainActivity)
    fun inject(testActivity: TestActivity)
}