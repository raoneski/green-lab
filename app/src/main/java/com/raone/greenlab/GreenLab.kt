package com.raone.greenlab

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.raone.greenlab.di.componets.DaggerGreenLabComponent
import com.raone.greenlab.di.componets.GreenLabComponent
import com.raone.greenlab.di.module.GreenLabModule

class GreenLab: Application() {
    lateinit var component: GreenLabComponent

    override fun onCreate() {
        super.onCreate()

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        instance = this
        setup()
    }

    private fun setup() {
        component = DaggerGreenLabComponent.builder()
            .greenLabModule(GreenLabModule(this)).build()
        component.inject(this)
    }


    fun getGreenLabComponent(): GreenLabComponent {
        return component
    }

    companion object {
        lateinit var instance: GreenLab private set
    }
}