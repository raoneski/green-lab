package com.raone.greenlab

import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.raone.greenlab.ui.view.GreenLabAlertDialog


inline fun Fragment.GreenLabAlertDialog(func: GreenLabAlertDialog.() -> Unit): AlertDialog =
    GreenLabAlertDialog(this.context!!).apply {
        func()
    }.create()

inline fun AppCompatActivity.GreenLabAlertDialog(func: GreenLabAlertDialog.() -> Unit): AlertDialog =
    GreenLabAlertDialog(this).apply {
        func()
    }.create()
