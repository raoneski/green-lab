package com.raone.greenlab.presenter

import com.raone.greenlab.GreenLab
import com.raone.greenlab.base.BasePresenter
import com.raone.greenlab.interactor.ISettingsPreferences
import com.raone.greenlab.interactor.SettingsInteractor
import com.raone.greenlab.view.ISettingsView
import javax.inject.Inject

class SettingsPresenter: BasePresenter<ISettingsView>(), ISettingsPreferences {

    @Inject
    lateinit var interactor: SettingsInteractor

    init {
        GreenLab.instance.getGreenLabComponent().inject(this)
    }

    override fun saveInt(key: String, value: Int) {
        interactor.saveInt(key, value)
    }

    override fun loadInt(key: String): Int {
        return interactor.loadInt(key)
    }

    override fun saveFloat(key: String, value: Float) {
        interactor.saveFloat(key, value)
    }

    override fun loadFloat(key: String): Float {
        return interactor.loadFloat(key)
    }

    override fun saveBoolean(key: String, value: Boolean) {
        interactor.saveBoolean(key, value)
    }

    override fun loadBoolean(key: String): Boolean {
        return interactor.loadBoolean(key)
    }

    override fun saveLong(key: String, value: Long) {
        interactor.saveLong(key, value)
    }

    override fun loadLong(key: String): Long {
        return interactor.loadLong(key)
    }

    override fun saveString(key: String, value: String) {
        interactor.saveString(key, value)
    }

    override fun loadString(key: String): String {
        return interactor.loadString(key)
    }

}